BRIC handbook
=============

This is the handbook of the 
`Brain Research and Imaging Center <https://www.plymouth.ac.uk/research/psychology/brain-research-and-imaging-centre>`_  of he university of Plymouth.

URL: https://brichandbook.readthedocs.io/en/latest/index.html

