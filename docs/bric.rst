========================
BRIC general information
========================

.. meta::
   :description lang=en: Discover the labs at BRIC and find some information on how make the best of them.


Contact information
-----------------------------------

BRIC director: Stephen Hall

Research Administrator: Kathryn Callicott

""""""""""""""""""

Access to BRIC
~~~~~~~~~~~~~~~

.. figure:: /_static/bric_xlarge_2100102.jpg
   :width: 300px
   :align: right

   BRIC building

Address: Research Way, Plymouth Science Park, Plymouth, PL6 8BU

* By bus: Bus 51, stop at Miller way & Research way. 
* By car: A limited number of parking space is available. 
Download a :download:`pdf map to BRIC <_static/CampusMapNorth_May21.pdf>`

""""""""""""""""""
