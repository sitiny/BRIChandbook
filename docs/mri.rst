.. _MRI_general:

================
MRI Lab general
================

MRI lab heads: Nadège Bault & Matt Roser

.. contents:: Table of Contents

Starting a new study
--------------------

Lab resources
~~~~~~~~~~~~~

Submitting a project
~~~~~~~~~~~~~~~~~~~~

Safety and ethics
------------------

Safety training
~~~~~~~~~~~~~~~

.. _safety:

Before being able to access the MRI lab, you will need to complete a safety training. Safety training sessions may be ran monthly or bi-monthly, depending on the needs, in small groups.
Please contact the lab heads (BRIC.MRI-lab.heads@plymouth.ac.uk).

Safety questionnaires must be filled by staff and research participants before they can enter the restricted MRI environment.

:download:`safety questionnaire - adults research participants <mri/BRIC MRI safety Adults questionnaire - research participants.pdf>`

:download:`safety questionnaire - staff (permit to work) <mri/BRIC MRI safety questionnaire - Staff (permit to work).pdf>`

Information & consent forms
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _templates:

Template information form in
:download:`.odt <mri/Information_form.odt>` and 
:download:`.docx <mri/Information_form.docx>`

Template consent form in 
:download:`.odt <mri/Consent_form.odt>` and 
:download:`.docx <mri/Consent_form.docx>`

General information for participants
-------------------

This :download:`general information form <mri/General_Information_MRI-lab.docx>` contains information for patients/participants about the MRI environment, how to prepare for a session and how to come to BRIC. Please add you contact information at the end of the form before sending it to your participants!


~~~~~~~~~~~~

Developing and testing a task
-----------------------------

