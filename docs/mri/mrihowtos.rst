.. _MRI_how_to:

==============
MRI Lab how to
==============


Start a new project
-------------------------------------------


Get ethics approval
-------------------------------------------

Please follow `this link <https://www.plymouth.ac.uk/research/governance/research-ethics-policy>`_  to get the general information you need to submit an ethics application at the University of Plymouth.

Template for consent and information forms can be found :ref:`here <templates>`


Pre-screen participants
-------------------------------------------

Researchers should pre-screen participants to decrease the rejection rate on the day of scanning. To do so, researchers can send the safety questionnaire to the prospective participant and ask them to fill it.
If the participant answers 'yes' to any of the exclusion question, the researcher should make sure the participant is confortable about discussing their answer before asking for more details. In case of doubt, please contact the MRI lab heads. 
The participant will go through the form again on the day of scanning with the responsible MRI operator.

:download:`safety questionnaire - adults research participants <BRIC MRI safety Adults questionnaire - research participants.pdf>`


Book the scanner
-------------------------------------------

`Booking system <https://resourcebooker.plymouth.ac.uk>`_  for University researchers. Once your project has been approved, please contact the MRI lab heads to be granted access as an MRI booker.